import com.mysql.jdbc.Driver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class DbCon {
    public PreparedStatement statement = null;
    public Connection connection = null;
    public ResultSet rs = null;

    public void connect() {
        try {
            DriverManager.registerDriver(new Driver());
            connection = DriverManager.getConnection(DbProp.url, DbProp.username, DbProp.password);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void close() {
        try {
            statement.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int checkState(int id) {
        try {
            connect();
            statement = connection.prepareStatement("select status from mailSender where id=" + id);
            rs = statement.executeQuery();
            rs.next();
            return rs.getInt(1);
        } catch (Exception e) {

        } finally {
            close();
        }
        return 0;
    }

    public List<MailSender> selectAll() {
        List<MailSender> mails = new ArrayList<MailSender>();
        try {
            connect();
            statement = connection.prepareStatement("Select * from mailSender where mail is not null");
            rs = statement.executeQuery();

            while (rs.next()) {
                mails.add(new MailSender(rs.getInt("id"), rs.getString("mail"), rs.getInt("status")));
            }
            return mails;
        } catch (Exception e) {
            e.printStackTrace();
            return mails;
        } finally {
            close();
        }
    }

    public void update(int id) {
        try {
            connect();
            statement = connection.prepareStatement("UPDATE mailSender SET status = 1  WHERE id=" + id);
            statement.executeUpdate();
            System.out.println(statement);
        } catch (Exception e) {
        } finally {
            close();
        }
    }

}
