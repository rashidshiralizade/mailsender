import org.apache.log4j.Logger;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class SendAttachmentInEmail {

    private static DbCon db = new DbCon();
    static final Logger logger = Logger.getLogger(SendAttachmentInEmail.class);

    public static void main(String[] args) throws Exception {


        try {
            int i = 0;
            for (MailSender mailSender : db.selectAll()) {
                if (db.checkState(mailSender.getId()) == 0) {
                    Properties prop = new Properties();
                    InputStream input = null;

                    try {
                        input = new FileInputStream("../mailSender.properties");

                        // load the properties file
                        prop.load(input);

                    } catch (IOException ex) {
                        logger.info("error happened prop file" + ex);
                    } finally {
                        if (input != null) {
                            try {
                                input.close();
                            } catch (IOException e) {
                                logger.info("error happened prop file" + e);
                            }
                        }
                    }
                    logger.info("I am in " + i++);

                    String from = prop.getProperty("from");

                    final String username = prop.getProperty("username");
                    final String password = prop.getProperty("password");

                    Properties props = new Properties();
                    props.put("mail.smtp.auth", "true");
                    props.put("mail.smtp.starttls.enable", prop.getProperty("tls"));
                    props.put("mail.smtp.host", prop.getProperty("host"));
                    props.put("mail.smtp.port", prop.getProperty("port"));

                    
                    Session session = Session.getInstance(props,
                            new javax.mail.Authenticator() {
                                protected PasswordAuthentication getPasswordAuthentication() {
                                    return new PasswordAuthentication(username, password);
                                }
                            });

                    try {
                        // Create a default MimeMessage object.
                        Message message = new MimeMessage(session);

                        // Set From: header field of the header.
                        message.setFrom(new InternetAddress(from));

                        // Set To: header field of the header.
                        message.setRecipients(Message.RecipientType.TO,
                                InternetAddress.parse(mailSender.getMail()));

                        // Set Subject: header field
                        message.setSubject(prop.getProperty("subject"));

                        // Create the message part
                        BodyPart messageBodyPart = new MimeBodyPart();

                        // Now set the actual message
                        messageBodyPart.setText(prop.getProperty("body"));

                        // Create a multipar message
                        Multipart multipart = new MimeMultipart();

                        // Set text message part
                        multipart.addBodyPart(messageBodyPart);

                        // Part two is attachment
                        messageBodyPart = new MimeBodyPart();
                        String filename = prop.getProperty("fileName");
                        DataSource source = new FileDataSource(filename);
                        messageBodyPart.setDataHandler(new DataHandler(source));
                        messageBodyPart.setFileName(filename);
                        multipart.addBodyPart(messageBodyPart);

                        // Send the complete message parts
                        message.setContent(multipart);

                        // Send message
                        Transport.send(message);

                        logger.info("Sent message successfully....");
                        System.out.println("Sent message successfully....");

                    } catch (MessagingException e) {
                        throw new RuntimeException(e);
                    }

                    db.update(mailSender.getId());
                }
            }
        } catch (Exception e) {
            logger.info("Error happened " + e);
        }
    }
}
