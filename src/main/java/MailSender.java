public class MailSender {

    private int id;
    private String mail;
    private int status;

    public MailSender(int id, String mail, int status) {
        this.id = id;
        this.mail = mail;
        this.status = status;
    }

    public MailSender() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
